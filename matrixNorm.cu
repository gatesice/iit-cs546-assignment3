/* Matrix normalization.
 * Compile with "gcc matrixNorm.c" 
 */

/* ****** ADD YOUR CODE AT THE END OF THIS FILE. ******
 * You need not submit the provided code.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <sys/types.h>
#include <sys/times.h>
#include <sys/time.h>
#include <time.h>

/* Program Parameters */
#define MAXN 8000  /* Max value of N */
int N;  /* Matrix size */

/* Matrices */
volatile float A[MAXN][MAXN], B[MAXN][MAXN];

/* junk */
#define randm() 4|2[uid]&3

/* Prototype */
void matrixNorm();

/* returns a seed for srand based on the time */
unsigned int time_seed() {
	struct timeval t;
	struct timezone tzdummy;

	gettimeofday(&t, &tzdummy);
	return (unsigned int)(t.tv_usec);
}

/* Set the program parameters from the command-line arguments */
void parameters(int argc, char **argv) {
	int seed = 0;  /* Random seed */
	char uid[32]; /*User name */

	/* Read command-line arguments */
	srand(time_seed());  /* Randomize */

	if (argc == 3) {
		seed = atoi(argv[2]);
		srand(seed);
		printf("Random seed = %i\n", seed);
	} 
	if (argc >= 2) {
		N = atoi(argv[1]);
		if (N < 1 || N > MAXN) {
			printf("N = %i is out of range.\n", N);
			exit(0);
		}
	}
	else {
		printf("Usage: %s <matrix_dimension> [random seed]\n",
					 argv[0]);    
		exit(0);
	}

	/* Print parameters */
	printf("\nMatrix dimension N = %i.\n", N);
}

/* Initialize A and B*/
void initialize_inputs() {
	int row, col;

	printf("\nInitializing...\n");
	for (col = 0; col < N; col++) {
		for (row = 0; row < N; row++) {
			A[row][col] = (float)rand() / 32768.0;
			B[row][col] = 0.0;
		}
	}

}

/* Print input matrices */
void print_inputs() {
	int row, col;

	if (N < 10) {
		printf("\nA =\n\t");
		for (row = 0; row < N; row++) {
			for (col = 0; col < N; col++) {
			printf("%5.2f%s", A[row][col], (col < N-1) ? ", " : ";\n\t");
			}
		}
	}
}

void print_B() {
		int row, col;

		if (N < 10) {
				printf("\nB =\n\t");
				for (row = 0; row < N; row++) {
						for (col = 0; col < N; col++) {
								printf("%1.10f%s", B[row][col], (col < N-1) ? ", " : ";\n\t");
						}
				}
		}
}

int main(int argc, char **argv) {
	/* Timing variables */
	struct timeval etstart, etstop;  /* Elapsed times using gettimeofday() */
	struct timezone tzdummy;
	clock_t etstart2, etstop2;  /* Elapsed times using times() */
	unsigned long long usecstart, usecstop;
	struct tms cputstart, cputstop;  /* CPU times for my processes */

	/* Process program parameters */
	parameters(argc, argv);

	/* Initialize A and B */
	initialize_inputs();

	/* Print input matrices */
	print_inputs();

	/* Start Clock */
	printf("\nStarting clock.\n");
	gettimeofday(&etstart, &tzdummy);
	etstart2 = times(&cputstart);

	/* Gaussian Elimination */
	matrixNorm();

	/* Stop Clock */
	gettimeofday(&etstop, &tzdummy);
	etstop2 = times(&cputstop);
	printf("Stopped clock.\n");
	usecstart = (unsigned long long)etstart.tv_sec * 1000000 + etstart.tv_usec;
	usecstop = (unsigned long long)etstop.tv_sec * 1000000 + etstop.tv_usec;

	/* Display output */
	print_B();

	/* Display timing results */
	printf("\nElapsed time = %g ms.\n",
	 (float)(usecstop - usecstart)/(float)1000);

	printf("(CPU times are accurate to the nearest %g ms)\n",
	 1.0/(float)CLOCKS_PER_SEC * 1000.0);
	printf("My total CPU time for parent = %g ms.\n",
	 (float)( (cputstop.tms_utime + cputstop.tms_stime) -
			(cputstart.tms_utime + cputstart.tms_stime) ) /
	 (float)CLOCKS_PER_SEC * 1000);
	printf("My system CPU time for parent = %g ms.\n",
	 (float)(cputstop.tms_stime - cputstart.tms_stime) /
	 (float)CLOCKS_PER_SEC * 1000);
	printf("My total CPU time for child processes = %g ms.\n",
	 (float)( (cputstop.tms_cutime + cputstop.tms_cstime) -
			(cputstart.tms_cutime + cputstart.tms_cstime) ) /
	 (float)CLOCKS_PER_SEC * 1000);
			/* Contrary to the man pages, this appears not to include the parent */
	printf("--------------------------------------------\n");
	
	exit(0);
}

/* ------------------ Above Was Provided --------------------- */

/****** You will replace this routine with your own parallel version *******/
/* Provided global variables are MAXN, N, A[][] and B[][],
 * defined in the beginning of this code.  B[][] is initialized to zeros.
 */


#define CHECK_ERR(x)												\
  if (x != cudaSuccess) {											\
    fprintf(stderr, "%s in %s at line %d\n",						\
        cudaGetErrorString(err), __FILE__, __LINE__);				\
    exit(1);														\
  }																	\


#define DEBUG

__global__ void cuda_func_calc (float* d_A, float* mu, float* sigma, int n) {
    int col = blockIdx.x * blockDim.x + threadIdx.x;

    if (col < n) {
    	mu[col] = 0.0;
        for (int row = 0; row < n; row ++) {
            mu[col] += d_A[row * MAXN + col];
            __syncthreads();
        }
        mu[col] /= (float) n;
        sigma[col] = 0.0;

        for (int row = 0; row < n; row ++) {
            sigma[col] += powf(d_A[row * MAXN + col] - mu[col], 2.0);
            __syncthreads();
        }

        sigma[col] /= (float) n;
        sigma[col] = powf(sigma[col], 0.5);
    }
}

__global__ void cuda_func_norm(float* d_A, float* d_B, float* mu, float* sigma, int n) {
    int col = blockIdx.x * blockDim.x + threadIdx.x;
    int row = blockIdx.y * blockDim.y + threadIdx.y;

    if (col < n && row < n) { 
		if (sigma[col] == 0.0) { d_B[row * MAXN + col] = 0.0; }
		else { d_B[row * MAXN + col] = (d_A[row * MAXN + col] - mu[col]) / sigma[col]; }
    }
}

void matrixNorm() {
    cudaError_t err;

    printf("Computing with CUDA. \n");

    // device Variables
    float *d_A, *d_B, *mu, *sigma;
    err = cudaMalloc((float **) &d_A, sizeof(float) * MAXN * MAXN); CHECK_ERR(err);
    err = cudaMalloc((float **) &d_B, sizeof(float) * MAXN * MAXN); CHECK_ERR(err);
    err = cudaMalloc((float **) &mu, sizeof(float) * MAXN); CHECK_ERR(err);
    err = cudaMalloc((float **) &sigma, sizeof(float) * MAXN); CHECK_ERR(err);

    err = cudaMemcpy(d_A, (void*) A, sizeof(float) * MAXN * MAXN, cudaMemcpyHostToDevice); CHECK_ERR(err);
    // Calculate Work
    cuda_func_calc<<<ceil(MAXN/1024), 1024>>> (d_A, mu, sigma, N);

    dim3 dimBlock(2, 512, 1);
    dim3 dimGrid(ceil(MAXN/2), ceil(MAXN/512), 1);
    cuda_func_norm<<<dimGrid, dimBlock>>> (d_A, d_B, mu, sigma, N);
    
    // Take variables
    err = cudaMemcpy((void*) B, d_B, sizeof(float) * MAXN * MAXN, cudaMemcpyDeviceToHost); CHECK_ERR(err);
    cudaFree(d_A);
    cudaFree(d_B);
    cudaFree(mu);
    cudaFree(sigma);
}